const express = require('express');
const router = express.Router();
const routesVersioning = require('express-routes-versioning')();
const paginationRouter = require('./v1/pagination');
const usersRouter = require('./v1/users');
const imagesRouter = require('./v1/images');

/*
* @name:: noMatchingVersionFound
* @category:: Callback function
* @description:: The specified API version is not implemented.
*/
function noMatchingVersionFound(req, res, next) {
  res.status(501).send('The specified API version is not implemented.');
}

/* GET API home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.use('/pagination', routesVersioning({ "1.0.0": paginationRouter }, noMatchingVersionFound));
router.use('/users', routesVersioning({ "1.0.0": usersRouter }, noMatchingVersionFound));
router.use('/images', routesVersioning({ "1.0.0": imagesRouter }, noMatchingVersionFound));

module.exports = router;
