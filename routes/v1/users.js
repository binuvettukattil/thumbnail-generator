const express = require('express');
const router = express.Router();

const authorize = require("../../middlewares/authorize");
const paginate = require("../../middlewares/paginate");
const {userValidator} = require('../../middlewares/user-validator');

const {
    signUp,
    signIn,
    signOut,
    getAllUsers,
    getUser,
} = require("../../controllers/user");

/* POST /users/sign-up */
router.post('/sign-up', userValidator, signUp);

/* POST /users/sign-in */
router.post('/sign-in', signIn);

/* POST /users/sign-out */
router.post('/sign-out', authorize, signOut);

/* GET users listing. */
router.get('/', authorize, paginate, getAllUsers);

/* GET user details. */
router.get('/profile', authorize, getUser);

module.exports = router;
