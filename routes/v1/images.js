const express = require('express');
const router = express.Router();
const multer = require('multer');
const config = require("config");

const authenticate = require("../../middlewares/authenticate");
const paginate = require("../../middlewares/paginate");
const { imageValidator } = require('../../middlewares/image-validator');

const UPLOAD_PATH = config.get("upload.path");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, UPLOAD_PATH)
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "image/png" || file.mimetype == "image/jpg" || file.mimetype == "image/jpeg") {
            cb(null, true);
        } else {
            cb(null, false);
            return cb(new Error('Only .png, .jpg and .jpeg format are supported right now!'));
        }
    }
});

const {
    getImages,
    getImage,
    uploadImage
} = require("../../controllers/image");

/* GET /images */
router.get('/', authenticate, paginate, getImages);

/* GET /images/view */
router.get('/view', authenticate, getImage);

/* POST /images */
router.post('/', imageValidator, authenticate, upload.single('image'), uploadImage);

module.exports = router;
