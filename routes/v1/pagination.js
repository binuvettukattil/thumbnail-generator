const express = require('express');
const router = express.Router();

const { pageValidator } = require('../../middlewares/page-validator');

const {
    getPagination,
    updatePagination
} = require("../../controllers/pagination");

/* GET /pagination */
router.get('/', getPagination);

/* POST /pagination */
router.post('/', pageValidator, updatePagination);

module.exports = router;
