test('Get pagination', async () => {
    const pagination = await getPagination();

    console.log("Pagination API response: ", pagination);

    expect(pagination).toEqual(10);

    async function getPagination() {
        let data = 10;

        return data;
    }
});
