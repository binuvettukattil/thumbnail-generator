const chalk = require('chalk');
const config = require("config");
const mailgun = require("mailgun-js");

const API_KEY = config.get("mail.api_key");
const DOMAIN = config.get("mail.domain");
const FROM = config.get("mail.from");

/*
* @name:: sendMail
* @category:: Helper
* @description:: Send email using mailgun service
*/
const sendMail = async (to, subject, message) => {
    const mg = mailgun({
        apiKey: API_KEY,
        domain: DOMAIN
    });

    const config = {
        from: FROM,
        to: to,
        subject: subject,
        text: message
    };

    mg.messages().send(config, function (error, body) {
        if (error) {
            console.log(chalk.red("Error sending email"));
            console.log(error);

            return false;
        }

        console.log(chalk.green("Email sent successfully"));

        return true;
    });
};

module.exports = { sendMail };
