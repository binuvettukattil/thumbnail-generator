const chalk = require('chalk');
const config = require("config");
const en = require("../nls/en");
const db = require("../models/index");
const User = db.User;

/*
* @name:: authenticate
* @category:: Middleware
* @description:: Authenticate if the current user using api_key.
*/
const authenticate = async (req, res, next) => {
    let apiKey = req.headers.api_key;

    if (apiKey && apiKey.length > 0) {
        let user = await User.findOne({
            where: {
                api_key: apiKey
            }
        });

        if (user) {
            req.api_key = apiKey;

            return next();
        } else {
            res.status(403).send({
                code: 403,
                status: "failed",
                message: en.ACCESS_DENIED,
                data: null
            });
        }
    } else {
        res.status(403).send({
            code: 403,
            status: "failed",
            message: en.ACCESS_DENIED,
            data: null
        });
    }
};

module.exports = authenticate;