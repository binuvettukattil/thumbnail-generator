const chalk = require('chalk');
const config = require("config");
const db = require("../models/index");
const Pagination = db.Pagination;

const ITEMS_PER_PAGE = config.get("pagination.items_per_page");;

/*
* @name:: paginate
* @category:: Middleware
* @description:: Get the items per page pagination value.
*/
const paginate = async (req, res, next) => {
    let pagination = await Pagination.findOne({
        where: {
            id: 1
        }
    });

    if (pagination) {
        req.items_per_page = pagination.items_per_page;
    } else {
        req.items_per_page = ITEMS_PER_PAGE;
    }

    return next();
};

module.exports = paginate;
