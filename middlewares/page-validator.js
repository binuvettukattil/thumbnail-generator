const joi = require("joi");

const schema = joi.object().keys({
    items_per_page: joi.number().required()
});

const pageValidator = async (req, res, next) => {
    try {
        const result = await schema.validateAsync(req.body);
        next();
    } catch (err) {
        res.status(400).json({ message: err.details });
    }
};

module.exports = { pageValidator };
