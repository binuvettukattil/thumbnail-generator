const joi = require("joi");

const schema = joi.object().keys({
    size: joi.number()
});

const imageValidator = async (req, res, next) => {
    try {
        const result = await schema.validateAsync(req.body);
        next();
    } catch (err) {
        res.status(400).json({ message: err.details });
    }
};

module.exports = { imageValidator };
