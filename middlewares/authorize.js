const chalk = require('chalk');
const config = require("config");
const jwt = require("jsonwebtoken");
const en = require("../nls/en");
const JWT_KEY = config.get("authorize.jwt_key");

/*
* @name:: authorize
* @category:: Middleware
* @description:: Check if the current user is authorized to perform the action or not.
*/
const authorize = async (req, res, next) => {
    let authorization = req.headers.authorization;

    if (authorization) {
        let tokenParts = authorization.split(' ');

        if (tokenParts.length == 2) {
            try {
                let token = jwt.verify(tokenParts[1], JWT_KEY);

                if (token) {
                    req.token = token;

                    console.log(req.token);
                    
                    return next();
                } else {
                    res.status(401).send({
                        code: 401,
                        status: "failed",
                        message: en.UNAUTHORIZED,
                        data: null
                    });
                }
            } catch (jsonWebTokenError) {
                res.status(401).send({
                    code: 401,
                    status: "failed",
                    message: en.UNAUTHORIZED,
                    data: null
                });
            }
        } else {
            res.status(401).send({
                code: 401,
                status: "failed",
                message: en.UNAUTHORIZED,
                data: null
            });
        }
    } else {
        res.status(401).send({
            code: 401,
            status: "failed",
            message: en.UNAUTHORIZED,
            data: null
        });
    }
};

module.exports = authorize;