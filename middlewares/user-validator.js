const joi = require("joi");

const schema = joi.object().keys({
    first_name: joi.string(),
    last_name: joi.string(),
    email: joi.string().email().required(),
    password: joi.string().min(8).required(),
});

const userValidator = async (req, res, next) => {
    try {
        const result = await schema.validateAsync(req.body);
        next();
    } catch (err) {
        res.status(400).json({ message: err.details });
    }
};

module.exports = { userValidator };
