'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [{
      first_name: "System",
      last_name: "Admin",
      email: "admin@whiterabbit.group",
      password: "$2b$10$cv6cfDRdmZfaDA1kdFdfduPx3IMMUiOwXUc9b6..U9VqSwxds0cJS",
      api_key: "1f4056bc525bcae97065bea926b2271a",
      role: 1,
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
