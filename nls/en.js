const en = {
    "PAGINATION_DETAILS": "Pagination details.",

    "SERVER_ERROR": "Sorry an error occurred on the server. Please try again.",

    "USERS_LIST": "Users list",
    "USER_ALREADY_EXISTS": "User with the same email already exists.",
    "USER_SIGNUP_SUCCESS": "User signed up successfully",
    "USER_SIGNED_OUT_SUCCESS": "Users signed out successfully",
    "USER_DETAILS": "User details.",
    "USER_LOGIN_FAILED": "User login failed.",
    "USER_DETAILS_NOT_FOUND": "User details not found.",
    "UNAUTHORIZED": "Unauthorized.",
    "ACCESS_DENIED": "Access denied. Please specify a valid API key",

    "FILE_UPLOADED_SUCCESSFULLY": "File has been uploaded successfully.",
    "FILE_FORMAT_NOT_SUPPORTED": "Only .png, .jpg and .jpeg format are supported right now.",
    "IMAGES_LIST": "Images list.",
    "IMAGE_DETAILS": "Image details.",
    "IMAGE_DETAILS_NOT_FOUND": "Image details not found.",
}

module.exports = en;