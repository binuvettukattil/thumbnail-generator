const chalk = require('chalk');
const config = require("config");
const bcrypt = require('bcrypt');
const md5 = require('md5');
const db = require("../models/index");
const User = db.User;
const jwt = require("jsonwebtoken");
const { sendMail } = require("../helpers/mailer");
const en = require("../nls/en");

const SALT_ROUNDS = config.get("secure.salt_rounds");
const JWT_KEY = config.get("authorize.jwt_key");

/*
* @name:: signUp
* @category:: Controller method
* @description:: User signup
*/
async function signUp(req, res) {
    console.log(chalk.green("POST /users/sign-up"));

    let user = await User.findOne({
        where: {
            email: req.body.email
        }
    });

    if (user) {
        res.status(409).send({
            code: 409,
            status: "failed",
            message: en.USER_ALREADY_EXISTS,
            data: null
        });
    } else {
        let encryptedPassword = await bcrypt.hash(req.body.password, SALT_ROUNDS);
        let apiKey = await md5(req.body.email + new Date().toString());
        let createdUser = await User.create({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            password: encryptedPassword,
            api_key: apiKey,
            role: 0
        });

        if (createdUser) {
            let user = await User.findOne({
                where: {
                    email: req.body.email
                },
                attributes: {
                    exclude: [
                        "password"
                    ],
                },
            });

            if (user) {
                await sendMail(user.email,
                    "Welcome to image thumbnail generation system.",
                    "You have successfully signed up for the image thumbnail generation system."
                );

                res.status(201).send({
                    code: 201,
                    status: "success",
                    message: en.USER_SIGNUP_SUCCESS,
                    data: user
                });
            } else {
                res.status(500).send({
                    code: 500,
                    status: "failed",
                    message: en.SERVER_ERROR,
                    data: null
                });
            }
        } else {
            res.status(500).send({
                code: 500,
                status: "failed",
                message: en.SERVER_ERROR,
                data: null
            });
        }
    }
}

/*
* @name:: signIn
* @category:: Controller method
* @description:: User sigin
*/
async function signIn(req, res) {
    console.log(chalk.green("POST /users/sign-in"));

    let user = await User.findOne({
        where: {
            email: req.body.email
        }
    });

    if (user) {
        let decryptedPassword = await bcrypt.compare(req.body.password, user.password);

        if (decryptedPassword) {
            let userToken = jwt.sign({ "id": user.id, "email": user.email, "api_key": user.api_key }, JWT_KEY);

            if (userToken) {
                user.dataValues.token = userToken;

                // Remove sensitive information
                delete user.dataValues.password;

                res.status(200).send({
                    code: 200,
                    status: "success",
                    message: en.USER_DETAILS,
                    data: user
                });
            } else {
                res.status(404).send({
                    code: 404,
                    status: "failed",
                    message: en.USER_LOGIN_FAILED,
                    data: null
                });
            }
        } else {
            res.status(404).send({
                code: 404,
                status: "failed",
                message: en.USER_LOGIN_FAILED,
                data: null
            });
        }
    } else {
        res.status(404).send({
            code: 404,
            status: "failed",
            message: en.USER_LOGIN_FAILED,
            data: null
        });
    }
}

/*
* @name:: signOut
* @category:: Controller method
* @description:: User signout
*/
async function signOut(req, res) {
    console.log(chalk.green("POST /users/sign-out"));

    let authUser = req.token;
    let user = await User.findOne({
        where: {
            id: authUser.id,
            email: authUser.email
        }
    });

    if (user) {
        // Use this block to reset api_key, fcm_token etc to null.

        res.status(200).send({
            code: 200,
            status: "success",
            message: en.USER_SIGNED_OUT_SUCCESS,
            data: null
        });
    } else {
        res.status(404).send({
            code: 404,
            status: "failed",
            message: en.USER_DETAILS_NOT_FOUND,
            data: null
        });
    }
}

/*
* @name:: getAllUsers
* @category:: Controller method
* @description:: Get all the users
*/
async function getAllUsers(req, res) {
    console.log(chalk.green("GET /users"));

    let pagination = { "page": req.query.page, "items_per_page": req.items_per_page };
    let authUser = req.token;
    let user = await User.findOne({
        where: {
            id: authUser.id,
            email: authUser.email
        }
    });

    if (user && user.role == 1) {
        let usersCount = await User.count({});
        let users = await User.findAll({
            where: {
            },
            attributes: [
                "id",
                "first_name",
                "last_name",
                "email",
                "role",
                "createdAt",
                "updatedAt"
            ],
            group: [
                "id",
                "createdAt",
            ],
            order: [["id", "ASC"]],
            offset: pagination.items_per_page * (pagination.page - 1),
            limit: req.items_per_page
        });

        if (usersCount > 0) {
            res.status(200).send({
                code: 200,
                status: "success",
                message: en.USERS_LIST,
                pagination: pagination,
                total_users: usersCount,
                data: users
            });
        } else {
            res.status(200).send({
                code: 200,
                status: "success",
                message: en.USERS_LIST,
                pagination: pagination,
                total_users: 0,
                data: []
            });
        }
    } else {
        res.status(401).send({
            code: 401,
            status: "failed",
            message: en.UNAUTHORIZED,
            data: null
        });
    }
}

/*
* @name:: getUser
* @category:: Controller method
* @description:: Get current user details
*/
async function getUser(req, res) {
    console.log(chalk.green("GET /users/profile"));

    let authUser = req.token;
    let user = await User.findOne({
        where: {
            id: authUser.id,
            email: authUser.email
        }
    });

    if (user) {
        // Remove sensitive information
        delete user.dataValues.password;

        res.status(200).send({
            code: 200,
            status: "success",
            message: en.USER_DETAILS,
            data: user
        });
    } else {
        res.status(404).send({
            code: 404,
            status: "failed",
            message: en.USER_DETAILS_NOT_FOUND,
            data: null
        });
    }
}

module.exports = {
    signUp,
    signIn,
    signOut,
    getAllUsers,
    getUser
};