const chalk = require('chalk');
const config = require("config");
const md5 = require('md5');
const sharp = require('sharp');
const db = require("../models/index");
const User = db.User;
const Image = db.Image;
const en = require("../nls/en");

const UPLOAD_PATH = config.get("upload.path");

/*
* @name:: getImages
* @category:: Controller method
* @description:: Get the images uplaoded by a user
*/
async function getImages(req, res) {
    console.log(chalk.green("GET /images"));

    const user = await User.findOne({
        where: {
            api_key: req.api_key
        }
    });

    if (!user) {
        res.status(403).send({
            code: 403,
            status: "failed",
            message: en.ACCESS_DENIED,
            data: null
        });
    }

    let page = parseInt(req.query.page) || 1;
    let pagination = { "page": page, "items_per_page": req.items_per_page };
    let imagesCount = await Image.count({
        where: {
            UserId: user.id
        }
    });
    let images = await Image.findAll({
        where: {
            UserId: user.id
        },
        attributes: {
            exclude: [
            ]
        },
        order: [["id", "ASC"]],
        offset: pagination.items_per_page * (pagination.page - 1),
        limit: req.items_per_page
    });

    if (imagesCount > 0) {
        res.status(200).send({
            code: 200,
            status: "success",
            message: en.IMAGES_LIST,
            pagination: pagination,
            total_images: imagesCount,
            data: images
        });
    } else {
        res.status(200).send({
            code: 200,
            status: "success",
            message: en.IMAGES_LIST,
            pagination: pagination,
            total_images: 0,
            data: []
        });
    }
}

/*
* @name:: uploadImage
* @category:: Controller method
* @description:: Upload image
*/
async function uploadImage(req, res) {
    console.log(chalk.green("POST /images"));
    const width = parseInt(req.body.width) || 200;
    const height = parseInt(req.body.height) || 200;
    const imageFile = req.file || null;
    const user = await User.findOne({
        where: {
            api_key: req.api_key
        }
    });

    if (!user) {
        res.status(403).send({
            code: 403,
            status: "failed",
            message: en.ACCESS_DENIED,
            data: null
        });
    }

    if (imageFile) {
        try {
            let imageType = null;
            let thumbnailImageFileName = md5(req.file.filename + new Date().toString());

            switch (req.file.mimetype) {
                case 'image/jpeg':
                    imageType = ".jpeg";
                    break;
                case 'image/jpg':
                    imageType = ".jpeg";
                    break;
                case 'image/png':
                    imageType = ".png";
                    break;
                default:
                    imageType = ".jpeg";
                    break;
            }

            await sharp(UPLOAD_PATH + req.file.filename)
                .resize(width, height, {
                    kernel: sharp.kernel.nearest,
                    fit: 'inside',
                    position: 'right top'
                })
                .toFile(UPLOAD_PATH + thumbnailImageFileName + imageType);

            let createdImage = await Image.create({
                original_url: UPLOAD_PATH + req.file.filename,
                thumbnail_url: UPLOAD_PATH + thumbnailImageFileName + imageType,
                mime_type: req.file.mimetype,
                UserId: user.id
            });

            if (createdImage) {
                res.status(201).send({
                    code: 201,
                    status: "success",
                    message: en.FILE_UPLOADED_SUCCESSFULLY,
                    data: createdImage
                });
            } else {
                res.status(500).send({
                    code: 500,
                    status: "failed",
                    message: en.SERVER_ERROR,
                    data: null
                });
            }
        } catch (err) {
            console.error(err);

            res.status(500).send({
                code: 500,
                status: "failed",
                message: en.SERVER_ERROR,
                data: null
            });
        }
    } else {
        res.status(500).send({
            code: 500,
            status: "failed",
            message: en.SERVER_ERROR,
            data: null
        });
    }
}

/*
* @name:: getImage
* @category:: Controller method
* @description:: Get image details
*/
async function getImage(req, res) {
    console.log(chalk.green("GET /images/view"));

    let id = parseInt(req.query.id) || 1;
    let image = await Image.findOne({
        where: {
            id: id
        }
    });

    if (image) {
        res.status(200).send({
            code: 200,
            status: "success",
            message: en.IMAGE_DETAILS,
            data: image
        });
    } else {
        res.status(404).send({
            code: 404,
            status: "failed",
            message: en.IMAGE_DETAILS_NOT_FOUND,
            data: null
        });
    }
}

module.exports = {
    getImages,
    getImage,
    uploadImage
};