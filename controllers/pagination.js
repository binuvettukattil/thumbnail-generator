const chalk = require('chalk');
const config = require("config");
const db = require("../models/index");
const Pagination = db.Pagination;
const en = require("../nls/en");

const ITEMS_PER_PAGE = config.get("pagination.items_per_page");

/*
* @name:: getPagination
* @category:: Controller method
* @description:: Get the pagination value
*/
async function getPagination(req, res) {
    console.log(chalk.green("GET /pagination"));

    let pagination = await Pagination.findOne({
        where: {
            id: 1
        }
    });

    if (pagination) {
        res.status(200).send({
            code: 200,
            status: "success",
            message: en.PAGINATION_DETAILS,
            data: pagination
        });
    } else {
        let createdPagination = await Pagination.create({
            items_per_page: ITEMS_PER_PAGE
        });

        if (createdPagination) {
            res.status(201).send({
                code: 201,
                status: "success",
                message: en.PAGINATION_DETAILS,
                data: createdPagination
            });
        } else {
            res.status(500).send({
                code: 500,
                status: "failed",
                message: en.SERVER_ERROR,
                data: null
            });
        }
    }
}

/*
* @name:: updatePagination
* @category:: Controller method
* @description:: Update the pagination value
*/
async function updatePagination(req, res) {
    console.log(chalk.green("POST /pagination"));
    console.log(chalk.green("Pagination value:: ", req.body.items_per_page));

    let pagination = await Pagination.findOne({
        where: {
            id: 1
        }
    });

    if (pagination) {
        let updatedPaginationCount = await Pagination.update({
            items_per_page: parseInt(req.body.items_per_page) || ITEMS_PER_PAGE
        }, {
            where: {
                id: 1
            }
        });

        if (updatedPaginationCount > 0) {
            let updatedPagination = await Pagination.findOne({
                where: {
                    id: 1
                }
            });

            if (updatedPagination) {
                res.status(200).send({
                    code: 200,
                    status: "success",
                    message: en.PAGINATION_DETAILS,
                    data: updatedPagination
                });
            } else {
                res.status(500).send({
                    code: 500,
                    status: "failed",
                    message: en.SERVER_ERROR,
                    data: null
                });
            }
        } else {
            res.status(500).send({
                code: 500,
                status: "failed",
                message: en.SERVER_ERROR,
                data: null
            });
        }
    } else {
        let createdPagination = await Pagination.create({
            items_per_page: ITEMS_PER_PAGE
        });

        if (createdPagination) {
            res.status(201).send({
                code: 201,
                status: "success",
                message: en.PAGINATION_DETAILS,
                data: createdPagination
            });
        } else {
            res.status(500).send({
                code: 500,
                status: "failed",
                message: en.SERVER_ERROR,
                data: null
            });
        }
    }
}

module.exports = {
    getPagination,
    updatePagination
};