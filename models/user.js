'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      User.hasMany(models.Image);
    }
  };
  User.init({
    first_name: { type: DataTypes.STRING },
    last_name: { type: DataTypes.STRING },
    email: { type: DataTypes.STRING, isEmail: true, unique: true, required: true },
    password: { type: DataTypes.STRING, required: true },
    api_key: { type: DataTypes.TEXT, required: true },
    role: { type: DataTypes.INTEGER, required: true },
  }, {
    sequelize,
    modelName: 'User'
  });
  return User;
};