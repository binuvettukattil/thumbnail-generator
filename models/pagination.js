'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Pagination extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Pagination.init({
    items_per_page: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Pagination'
  });
  return Pagination;
};