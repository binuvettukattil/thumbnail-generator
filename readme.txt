>> Install all node modules
npm install

>> Configure the database credentials
The database credentials are configured on the file config/config.json

>> Create the database
npx sequelize-cli db:create

>> Create the tables by running migration scripts
npx sequelize-cli db:migrate

>> Seed the default data to master tables. This command will create the admin user.
npx sequelize-cli db:seed:all

>> Bootstrap the application server in development mode
nodemon www (development mode)
 
or

>> Bootstrap the application server in production mode
pm2 start ecosystem.config.js --env production

>> Postman collection is attached with the project.
Thumbnail Generator.postman_collection.json